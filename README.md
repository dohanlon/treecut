# TreeCut

Convert a Scikit-Learn decision tree into a single cut string for Pandas or ROOT.

![](example/dt.png)

Usage
----

Instantiate a Scikit-Learn `DecisionTreeClassifier`:

```python
from sklearn import tree

dt = tree.DecisionTreeClassifier(max_depth = 5, ccp_alpha = 5E-4)
```

Fit the tree on some data:

```python
signal = np.random.multivariate_normal([0.0, 0.0], [[2 ** 2, 0], [0, 2 ** 2]], size = 10000)
bkg = np.random.multivariate_normal([2.0, 2.0], [[2 ** 2, 0], [0, 2 ** 2]], size = 10000)

data = np.concatenate([signal, bkg])
target = np.concatenate([np.ones(len(signal)), np.zeros(len(bkg))])

dt.fit(data, target)
```

Build an object representing the decision tree branches as cuts on the data. Feature names are used in the cut strings, so should be the same order as the data passed to the `DecisionTreeClassifier`:

```python
from TreeCuts import buildCuts

cuts = buildCuts(dt, featureNames = ['x', 'y'])
```

Get cut string that corresponds to the syntax for Pandas queries (i.e., with Python boolean operator syntax), for the target class `1`. Also supports ROOT style (i.e., C++ syntax) with the option `isROOT = True`. By construction the string for class `0` is the complement for class `1`.

```python
cutStr = cuts.cutString(target = 1)

'''
cutStr = ('( x <= 2.6617  and  y <= 2.7571  and  y > -1.3389 ) or ( x <= 2.6617  and  y '\
 '> 2.7571  and  x <= -0.8548 ) or ( x > 2.6617  and  y <= -0.4914  and  y <= '\
 '-1.7017 ) ')
'''
```

For deeper decision trees the generated string can be quite large and exceed the memory/stack limit in Pandas. In this case, a Pandas `DataFrame` can be passed to `cuts.applyCuts(...)` to apply cuts iteratively on the `DataFrame` and return the result:

```python
df = pd.DataFrame({'x' : testData[:,0], 'y' : testData[:,1]})

cutsApplied = cuts.applyCuts(df, target = 1)
```
