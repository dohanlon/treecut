import matplotlib.pyplot as plt

from matplotlib import rcParams
import matplotlib as mpl

mpl.use("Agg")

plt.style.use(["seaborn-whitegrid", "seaborn-ticks"])
import matplotlib.ticker as plticker

rcParams["figure.figsize"] = 12, 8
rcParams["axes.facecolor"] = "FFFFFF"
rcParams["savefig.facecolor"] = "FFFFFF"
rcParams["figure.facecolor"] = "FFFFFF"

rcParams["xtick.direction"] = "in"
rcParams["ytick.direction"] = "in"

rcParams["mathtext.fontset"] = "cm"
rcParams["mathtext.rm"] = "serif"

rcParams.update({"figure.autolayout": True})

from matplotlib.colors import ListedColormap

import string

import seaborn as sns

import numpy as np

np.random.seed(42)

from sklearn import tree

from pprint import pprint

import pandas as pd

from TreeCuts import buildCuts


def makeDecisionBoundaryPlot(cutStr, signal, bkg, xrange, yrange):

    x_min, x_max = xrange
    y_min, y_max = yrange

    plot_step = 0.02

    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, plot_step), np.arange(y_min, y_max, plot_step)
    )

    plotData = np.c_[xx.ravel(), yy.ravel()]

    plotDF = pd.DataFrame({"a": plotData[:, 0], "b": plotData[:, 1]})

    # Deal with the fact that I can't set on a query
    ones = plotDF.query(cutStr).copy()
    zeros = plotDF.query(f"not({cutStr})").copy()
    ones["z"] = 1
    zeros["z"] = 0

    # Sort so that these match meshgrid
    plotDF = pd.concat([zeros, ones]).sort_values(by=["b", "a"])

    palette = sns.color_palette("Paired")

    colours = [palette[0], palette[2]]
    my_cmap = ListedColormap(sns.color_palette(colours).as_hex())

    Z = plotDF["z"].values
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=my_cmap)

    plt.plot(bkg[:, 0], bkg[:, 1], ".", color=palette[1], alpha=1.0)
    plt.plot(signal[:, 0], signal[:, 1], ".", color=palette[3], alpha=1.0)

    plt.contour(xx, yy, Z, linewidths=2.0, zorder=5, colors="white")

    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)

    plt.xlabel("x", fontsize=24)
    plt.ylabel("y", fontsize=24)

    plt.savefig("dt_tree.pdf")
    plt.savefig("dt_tree.png")
    plt.clf()


def makeSKLearnBoundaryPlot(dt, xrange, yrange):

    x_min, x_max = xrange
    y_min, y_max = yrange

    xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01), np.arange(y_min, y_max, 0.01))

    Z = dt.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    cs = plt.contourf(xx, yy, Z, cmap=plt.cm.RdYlBu)

    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)

    plt.xlabel("x", fontsize=24)
    plt.ylabel("y", fontsize=24)

    plt.savefig("dt_sklearn.png")
    plt.clf()


def testCutStrDecisions(cutStr, dt, df, target):

    cut = df.query(cutStr)
    notCut = df.query(f"not({cutStr})")

    predCut = dt.predict(cut)
    predNotCut = dt.predict(notCut)

    assert len(cut) + len(notCut) == len(df)

    assert np.all(np.equal(predCut, target))
    assert np.all(np.equal(predNotCut, int(not target)))

    return cut, notCut, predCut, predNotCut


def testDecisions(dt, cuts, cutStr, ranges):

    cutStr0, cutStr1 = cutStr

    testData = np.stack([np.random.uniform(r[0], r[1], size=1000) for r in ranges], -1)

    chars = string.ascii_lowercase
    df = pd.DataFrame({chars[i]: testData[:, i] for i in range(len(ranges))})

    treeDepth = dt.get_depth()

    # Test for target = 1, complement for target = 0
    if treeDepth < 7:
        testCutStrDecisions(cutStr1, dt, df, 1)

    # Test for target = 0, complement for target = 1
    if treeDepth < 7:
        testCutStrDecisions(cutStr0, dt, df, 0)

    # Test applying on DataFrame

    onesApply = cuts.applyCuts(df, target=1)
    zerosApply = cuts.applyCuts(df, target=0)

    assert len(onesApply) + len(zerosApply) == len(df)

    decisionsZeroApply = dt.predict(zerosApply)
    decisionsOneApply = dt.predict(onesApply)

    assert np.all(np.equal(decisionsZeroApply, 0))
    assert np.all(np.equal(decisionsOneApply, 1))


def test2DDeep():

    signal = np.random.multivariate_normal(
        [0.0, 0.0], [[2 ** 2, 0], [0, 2 ** 2]], size=10000
    )
    bkg1 = np.random.multivariate_normal(
        [2.0, 2.0], [[2 ** 2, 0], [0, 2 ** 2]], size=10000
    )
    bkg2 = np.random.multivariate_normal(
        [-2.0, -2.0], [[1 ** 2, 0], [0, 1 ** 2]], size=5000
    )
    bkg = np.concatenate([bkg1, bkg2])

    data = np.concatenate([signal, bkg])
    target = np.concatenate([np.ones(len(signal)), np.zeros(len(bkg))])

    dt = tree.DecisionTreeClassifier(max_depth=6)

    dt.fit(data, target)

    cuts = buildCuts(dt, featureNames=["a", "b"])

    cutStr0 = cuts.cutString(target=0)
    cutStr1 = cuts.cutString(target=1)

    pprint(cutStr1)

    x_min, x_max = data[:, 0].min() - 1, data[:, 0].max() + 1
    y_min, y_max = data[:, 1].min() - 1, data[:, 1].max() + 1

    testDecisions(dt, cuts, [cutStr0, cutStr1], [(x_min, x_max), (y_min, y_max)])


def test2D():

    signal = np.random.multivariate_normal(
        [0.0, 0.0], [[2 ** 2, 0], [0, 2 ** 2]], size=10000
    )
    bkg1 = np.random.multivariate_normal(
        [2.0, 2.0], [[2 ** 2, 0], [0, 2 ** 2]], size=10000
    )
    bkg2 = np.random.multivariate_normal(
        [-2.0, -2.0], [[1 ** 2, 0], [0, 1 ** 2]], size=5000
    )
    bkg = np.concatenate([bkg1, bkg2])

    data = np.concatenate([signal, bkg])
    target = np.concatenate([np.ones(len(signal)), np.zeros(len(bkg))])

    dt = tree.DecisionTreeClassifier(max_depth=5, ccp_alpha=5e-4)

    dt.fit(data, target)

    cuts = buildCuts(dt, featureNames=["a", "b"])

    cutStr0 = cuts.cutString(target=0)
    cutStr1 = cuts.cutString(target=1)

    pprint(cutStr1)

    x_min, x_max = data[:, 0].min() - 1, data[:, 0].max() + 1
    y_min, y_max = data[:, 1].min() - 1, data[:, 1].max() + 1

    testDecisions(dt, cuts, [cutStr0, cutStr1], [(x_min, x_max), (y_min, y_max)])

    makeDecisionBoundaryPlot(cutStr1, signal, bkg, (x_min, x_max), (y_min, y_max))
    makeSKLearnBoundaryPlot(dt, (x_min, x_max), (y_min, y_max))


def test3D():

    signal = np.random.multivariate_normal(
        [0.0, 0.0, 0.0], [[2 ** 2, 0, 0], [0, 2 ** 2, 0], [0, 0, 2 ** 2]], size=10000
    )
    bkg1 = np.random.multivariate_normal(
        [2.0, 2.0, 2.0], [[2 ** 2, 0, 0], [0, 2 ** 2, 0], [0, 0, 2 ** 2]], size=10000
    )
    bkg2 = np.random.multivariate_normal(
        [-2.0, -2.0, -2.0], [[1 ** 2, 0, 0], [0, 1 ** 2, 0], [0, 0, 1 ** 2]], size=5000
    )
    bkg = np.concatenate([bkg1, bkg2])

    data = np.concatenate([signal, bkg])
    target = np.concatenate([np.ones(len(signal)), np.zeros(len(bkg))])

    dt = tree.DecisionTreeClassifier(max_depth=5, ccp_alpha=5e-4)

    dt.fit(data, target)

    cuts = buildCuts(dt, featureNames=["a", "b", "c"])

    cutStr0 = cuts.cutString(target=0)
    cutStr1 = cuts.cutString(target=1)

    pprint(cutStr1)

    x_min, x_max = data[:, 0].min() - 1, data[:, 0].max() + 1
    y_min, y_max = data[:, 1].min() - 1, data[:, 1].max() + 1
    z_min, z_max = data[:, 2].min() - 1, data[:, 2].max() + 1

    testDecisions(
        dt, cuts, [cutStr0, cutStr1], [(x_min, x_max), (y_min, y_max), (z_min, z_max)]
    )


def testND(d=16):

    signal = np.random.uniform(0, 1, size=(1000, d))
    bkg = np.random.uniform(0, 1, size=(1000, d))

    data = np.concatenate([signal, bkg])
    target = np.concatenate([np.ones(len(signal)), np.zeros(len(bkg))])

    dt = tree.DecisionTreeClassifier(max_depth=6)

    dt.fit(data, target)

    cuts = buildCuts(dt, featureNames=string.ascii_lowercase[:d])

    cutStr0 = cuts.cutString(target=0)
    cutStr1 = cuts.cutString(target=1)

    pprint(cutStr1)

    ranges = [(0, 1) for _ in range(d)]

    testDecisions(dt, cuts, [cutStr0, cutStr1], ranges)


if __name__ == "__main__":
    testND()
