import copy

import numpy as np
import pandas as pd

from sklearn import tree


class Cut(object):
    def __init__(
        self,
        target=-1,
        leafID=-1,
        values=[],
        features=[],
        signs=[],
        featureNames=[],
        isROOT=False,
    ):

        self.target = target
        self.leafID = leafID
        self.values = values.copy()
        self.features = features.copy()
        self.signs = signs.copy()
        self.featureNames = featureNames.copy()
        self.separator = " and " if not isROOT else " && "

    def __getitem__(self, sliced):

        c = Cut(
            self.target,
            self.leafID,
            self.values[sliced],
            self.features[sliced],
            self.signs[sliced],
            self.featureNames,
        )

        c.separator = self.separator

        return c

    def copy(self):
        return copy.deepcopy(self)

    def updateSign(self, childrenLeft, childrenRight, i):

        # This is a left node, previous was '<'
        if i in childrenLeft:

            self.signs.append("<=")

        elif i in childrenRight:

            # Left will always come first, if so, remove it
            if "<=" not in self.signs[-1]:
                self.signs.append(">")
            else:
                self.signs = self.signs[:-1]
                self.signs.append(">")

    def cutString(self):

        cutStr = []
        for i in range(len(self.values)):
            s = f"{self.featureNames[self.features[i]]} {self.signs[i]} {self.values[i]}"
            cutStr.append(s)

        return f" {self.separator} ".join(cutStr)

    def __str__(self):
        return f"{self.target}, {self.values}, {self.features}, {self.signs}"

    def __repr__(self):
        return self.__str__()


class CutSet(object):
    def __init__(self, cuts=[], isROOT=False):

        self.cuts = cuts.copy()  # So we don't end up modifying the default arg...
        self.separator = "or" if not isROOT else "||"

    def append(self, cut):

        self.cuts.append(cut)

    def __getitem__(self, key):

        return self.cuts[key]

    def copy(self):
        return copy.deepcopy(self)

    def cutString(self, target=0):

        str = ""

        for c in self.cuts:
            if c.target == target:
                str += f"( {c.cutString()} ) {self.separator} "

        str = str[: -(len(self.separator) + 1)]

        return str

    def applyCuts(self, df, target=0):

        # Iteratively apply cuts and combine the datasets
        # to avoid running of of memory with very large strings

        dfs = []

        for c in self.cuts:

            if c.target == target:
                dfs.append(df.query(c.cutString()))

        newDF = pd.concat(dfs)

        return newDF


def traverseTree(decisionTree):

    # From https://scikit-learn.org/stable/auto_examples/tree/plot_unveil_tree_structure.html

    n_nodes = decisionTree.tree_.node_count
    children_left = decisionTree.tree_.children_left
    children_right = decisionTree.tree_.children_right

    # The tree structure can be traversed to compute various properties such
    # as the depth of each node and whether or not it is a leaf.
    node_depth = np.zeros(shape=n_nodes, dtype=np.int64)
    is_leaves = np.zeros(shape=n_nodes, dtype=bool)
    stack = [(0, -1)]  # seed is the root node id and its parent depth

    while len(stack) > 0:
        node_id, parent_depth = stack.pop()
        node_depth[node_id] = parent_depth + 1

        # If we have a test node
        if children_left[node_id] != children_right[node_id]:
            stack.append((children_left[node_id], parent_depth + 1))
            stack.append((children_right[node_id], parent_depth + 1))
        else:
            is_leaves[node_id] = True

    return node_depth, is_leaves


def buildCuts(decisionTree, featureNames=[], thresholdRounding=4, isROOT=False):

    assert type(decisionTree) == tree.DecisionTreeClassifier

    n_nodes = decisionTree.tree_.node_count

    if n_nodes == 0:
        print("WARNING: Tree has no nodes. Nothing to do here.")
        return None

    children_left = decisionTree.tree_.children_left
    children_right = decisionTree.tree_.children_right
    feature = decisionTree.tree_.feature
    threshold = decisionTree.tree_.threshold
    value = decisionTree.tree_.value

    if not featureNames:
        print("WARNING: No feature names specified. Will continue with indices.")
        featureNames = list(map(str, np.unique(feature)))

    threshold = list(map(lambda x: round(x, thresholdRounding), threshold))

    node_depth, is_leaves = traverseTree(decisionTree)

    cut = Cut(featureNames=featureNames, isROOT=False)
    cutSet = CutSet(isROOT=False)

    for i in range(n_nodes):
        if is_leaves[i]:

            cut = cut[: node_depth[i]]

            cut.updateSign(children_left, children_right, i)

            cut.target = np.argmax(value[i][0])  # 1 output
            cut.leafID = i

            cutSet.append(cut.copy())

        else:
            cut = cut[: node_depth[i]]

            cut.updateSign(children_left, children_right, i)

            cut.values.append(threshold[i])
            cut.features.append(feature[i])

    return cutSet
